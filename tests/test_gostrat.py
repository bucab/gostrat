from tempfile import NamedTemporaryFile
import os

def test_download_go_cli(monkeypatch,request):

    from gostrat import main

    with NamedTemporaryFile() as obo :
        with NamedTemporaryFile() as gene2go :
            main([
                'download_go',
                '--obo={}'.format(obo.name),
                '--gene2go={}'.format(gene2go.name)
            ])
            assert open(obo.name).read() != 0
            assert open(gene2go.name).read() != 0

def test_gmt_cli(monkeypatch,request):

    import csv
    import goatools.base

    def download_go_basic_obo(**kwargs):
        return os.path.join(request.fspath.dirname,'go-example.obo')
    monkeypatch.setattr(
            goatools.base,
            'download_go_basic_obo',
            download_go_basic_obo
    )

    def download_ncbi_associations(**kwargs):
        return os.path.join(request.fspath.dirname,'gene2go-example')
    monkeypatch.setattr(
            goatools.base,
            'download_ncbi_associations',
            download_ncbi_associations
    )

    from gostrat import main

    with NamedTemporaryFile() as f :
        main(['generate_gmt','-o',f.name])
        assert os.path.exists(
                   os.path.join(
                       request.fspath.dirname,'gene2go-example_genemap.csv'
                   )
               )
        # not all of the go terms have annotations, and thus do not appear
        # in the gmt, check for that
        num_goid = 0
        with open(download_ncbi_associations()) as g2g_f :
            goids = set([
                _[2] for _ in csv.reader(g2g_f,delimiter='\t')
                if _[0] == '9606']
            )
            num_goid = len(goids)
        with open(f.name) as gmt_f :
            assert num_goid == len([_ for _ in gmt_f if len(_) != 0])

    # alternate taxonomy ID
    with NamedTemporaryFile() as f :
        mouse_taxid = '10090'
        main(['generate_gmt','-o',f.name,'-t',mouse_taxid])

        # not all of the go terms have annotations, and thus do not appear
        # in the gmt, check for that
        num_goid = 0
        with open(download_ncbi_associations()) as g2g_f :
            goids = set([
                _[2] for _ in csv.reader(g2g_f,delimiter='\t')
                if _[0] == mouse_taxid
            ])
            num_goid = len(goids)
        with open(f.name) as gmt_f :
            assert num_goid == len([_ for _ in gmt_f if len(_) != 0])

    # ensembl gene id type
    with NamedTemporaryFile() as f :
        main(['generate_gmt','-o',f.name,'-i','ensembl.gene'])
        assert os.path.exists(
                   os.path.join(
                       request.fspath.dirname,'gene2go-example_genemap.csv'
                   )
               )
        # not all of the go terms have annotations, and thus do not appear
        # in the gmt, check for that
        num_goid = 0
        with open(download_ncbi_associations()) as g2g_f :
            goids = set([
                _[2] for _ in csv.reader(g2g_f,delimiter='\t')
                if _[0] == '9606'
            ])
            num_goid = len(goids)
        with open(f.name) as gmt_f :
            assert num_goid == len([_ for _ in gmt_f if len(_) != 0])

def test_strat_cli(monkeypatch,request):

    import csv
    import goatools.base

    def download_go_basic_obo(**kwargs):
        return os.path.join(request.fspath.dirname,'go-example.obo')
    monkeypatch.setattr(
            goatools.base,
            'download_go_basic_obo',
            download_go_basic_obo
    )

    def download_ncbi_associations(**kwargs):
        return os.path.join(request.fspath.dirname,'gene2go-example')
    monkeypatch.setattr(
            goatools.base,
            'download_ncbi_associations',
            download_ncbi_associations
    )

    from gostrat import main

    gsea_a_fn = os.path.join(request.fspath.dirname,'gsea_a.csv')
    gsea_b_fn = os.path.join(request.fspath.dirname,'gsea_b.csv')

    # single file
    with NamedTemporaryFile() as f :
        # default -f < 0.05 should leave only 1 result
        main(['stratify','-o',f.name,gsea_a_fn])
        with open(f.name) as strat_f :
            assert len([_ for _ in strat_f if len(_) != 0]) == 2
 
        # default -f < 0.1 should leave 5 results
        main(['stratify','-o',f.name,'-f','0.1',gsea_a_fn])
        with open(f.name) as strat_f :
            assert len([_ for _ in strat_f if len(_) != 0]) == 6

        # default -f < 0.2 should leave 14 results
        main(['stratify','-o',f.name,'-f','0.2',gsea_a_fn])
        with open(f.name) as strat_f :
            out = [_ for _ in strat_f if len(_) != 0]
            assert len(out) == 15

        # default -f < 0 with -c stat should leave 5 results
        main(['stratify','-o',f.name,'-f','0','-c','stat',gsea_a_fn])
        with open(f.name) as strat_f :
            assert len([_ for _ in strat_f if len(_) != 0]) == 6

        # default -f < 0 with -c stat and --invert should leave 8 results
        main(['stratify','-o',f.name,'-f','0','-c','stat','--invert',gsea_a_fn])
        with open(f.name) as strat_f :
            out = [_ for _ in strat_f if len(_) != 0]
            assert len(out) == 10

        # default -f < 0 with -c stat and --abs should leave 0 results
        main(['stratify','-o',f.name,'-f','0','-c','stat','--abs',gsea_a_fn])
        with open(f.name) as strat_f :
            out = [_ for _ in strat_f if len(_) != 0]
            assert len(out) == 1

        # default -f < 0 with -c stat, --abs, and --invert should leave 14 results
        main(['stratify','-o',f.name,'-f','0','-c','stat','--abs','--invert',gsea_a_fn])
        with open(f.name) as strat_f :
            out = [_ for _ in strat_f if len(_) != 0]
            assert len(out) == 15

        # if go term in input file is missing from obo
        gsea_missing_fn = os.path.join(request.fspath.dirname,'gsea_missing_go.csv')
        main(['stratify','-o',f.name,'-f','0','-c','stat','--abs','--invert',gsea_missing_fn])
        with open(f.name) as strat_f :
            out = [_ for _ in strat_f if len(_) != 0]
            assert len(out) == 16

    # two files
    with NamedTemporaryFile() as f :
        # default -f < 0.05 should leave 4 results
        main(['stratify','-o',f.name,gsea_a_fn,gsea_b_fn])
        with open(f.name) as strat_f :
            assert len([_ for _ in strat_f if len(_) != 0]) == 5
 
        # default -f < 0.1 should leave 7 results
        main(['stratify','-o',f.name,'-f','0.1',gsea_a_fn,gsea_b_fn])
        with open(f.name) as strat_f :
            assert len([_ for _ in strat_f if len(_) != 0]) == 8

def test_plotconfig_cli(monkeypatch,request) :

    import goatools.base

    def download_go_basic_obo(**kwargs):
        return os.path.join(request.fspath.dirname,'go-example.obo')
    monkeypatch.setattr(
            goatools.base,
            'download_go_basic_obo',
            download_go_basic_obo
    )

    def download_ncbi_associations(**kwargs):
        return os.path.join(request.fspath.dirname,'gene2go-example')
    monkeypatch.setattr(
            goatools.base,
            'download_ncbi_associations',
            download_ncbi_associations
    )

    from gostrat import main

    gsea_a_fn = os.path.join(request.fspath.dirname,'gsea_a.csv')
    gsea_b_fn = os.path.join(request.fspath.dirname,'gsea_b.csv')

    with NamedTemporaryFile() as g_f :
        # default -f < 0.05 should leave 4 results
        main(['stratify','-g',g_f.name,gsea_a_fn,gsea_b_fn])
        main(['plotconfig','--json',g_f.name])

def test_plot_cli(monkeypatch, request) :

    import goatools.base

    def download_go_basic_obo(**kwargs):
        return os.path.join(request.fspath.dirname,'go-example.obo')
    monkeypatch.setattr(
            goatools.base,
            'download_go_basic_obo',
            download_go_basic_obo
    )

    def download_ncbi_associations(**kwargs):
        return os.path.join(request.fspath.dirname,'gene2go-example')
    monkeypatch.setattr(
            goatools.base,
            'download_ncbi_associations',
            download_ncbi_associations
    ) 
    from gostrat import main

    gsea_a_fn = os.path.join(request.fspath.dirname,'gsea_a.csv')
    gsea_b_fn = os.path.join(request.fspath.dirname,'gsea_b.csv')

    # two files
    with NamedTemporaryFile() as g_f :
        # default -f < 0.05 should leave 4 results
        main(['stratify','-g',g_f.name,gsea_a_fn,gsea_b_fn])
        main(['plot',g_f.name])

